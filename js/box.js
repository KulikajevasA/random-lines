import Vector from "./vector";


class Box {
    /**
     * 
     * @param {Vector} min 
     * @param {Vector} max 
     */
    constructor(min, max) {
        this.min = min;
        this.max = max;

        Object.freeze(this);
    }

    getCenter() { return this.min.add(this.max).mulScalar(0.5); }
    getSize() { return this.max.sub(this.min); }
    getRadius() { return this.getSize().length() * 0.5; }
}

export default Box;
export { Box };