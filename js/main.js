import getCanvas from "./canvas";
import Vector from "./vector";

import DATA from "./data";
import createNoiseModel from "./noise-model";

import * as Content from "./content";

function getData(url) {
    return new Promise(resolve => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', url || 'content/data.json');
        xhr.send();
        xhr.onload = function () {
            if (xhr.status !== 200) {
                alert(`Error ${xhr.status}: ${xhr.statusText}`);
            } else {
                let res = JSON.parse(xhr.response)
                resolve(res.data)
            }
        };
        xhr.onerror = function () {
            alert("Request failed");
        };
    })
}

//getData().then((DATA)=> {
const canvas = getCanvas("canvas");

const seed = 0x1337;
// const seed = Math.random();

const gridDims = 128;

const TWO_PI = Math.PI * 2;
const { ctx2d } = canvas;

const untraversable = [0.3, 0.3];

const noiseModel = createNoiseModel({ seed, gridDims, untraversable });

const curves = noiseModel.curves;
const points = noiseModel.points;

let pointsLeft = DATA.length;
const totalLength = curves.reduce((acc, [, value]) => acc + value, 0);
const pointDistance = totalLength / pointsLeft;
const imageSize = 10;

function drawCircle({ x, y }, radius, color, fill) {
    ctx2d.beginPath();
    ctx2d.strokeStyle = color;
    ctx2d.fillStyle = fill;
    ctx2d.lineWidth = 5;
    ctx2d.arc(x, y, radius, 0, TWO_PI);
    ctx2d.fill();
    ctx2d.stroke();
}

function drawHexagon(x, y, size, color) {
    ctx2d.beginPath();
    for (let side = 0; side < 7; side++) {
        ctx2d.lineTo(x + size * Math.cos(side * 2 * Math.PI / 6), y + size * Math.sin(side * 2 * Math.PI / 6));
    }

    ctx2d.fillStyle = color;
    ctx2d.fill();
}

function redraw() {
    const center = canvas.size * 0.5, origin = new Vector(center, center);
    const radius = center * 0.9;
    const cellSize = (radius * 2) / gridDims;

    canvas.erase();
    drawCircle(origin, radius, "transparent", "#0b0458");

    points.forEach(points => {
        const color = "#808080";

        const a = points[0]
            .mulScalar(cellSize)
            .add(origin)
            .subScalar(radius);
        ctx2d.beginPath();
        ctx2d.moveTo(...a.toArray());

        for (let i = 1, len = points.length; i < len; i++) {
            const b = points[i]
                .mulScalar(cellSize)
                .add(origin)
                .subScalar(radius);

            ctx2d.lineTo(...b.toArray());
        }

        ctx2d.strokeStyle = color;
        ctx2d.lineWidth = 3;
        ctx2d.stroke();
    });


    let pointIndex = 0;


    curves
        .sort(([, a], [, b]) => a - b)
        .forEach(([curve, lenCurve], index, array) => {
            const curvePoints = Math.floor(lenCurve / pointDistance);
            const curvePointsActual = array.length === index + 1
                ? pointsLeft
                : curvePoints & 1
                    ? curvePoints
                    : curvePoints > 2
                        ? curvePoints + 1
                        : 1

            pointsLeft -= curvePointsActual;

            const stepSize = 1 / (curvePointsActual + 1);

            for (let i = 0; i < curvePointsActual; i++) {
                const position = curve.getPointAt(stepSize + i * stepSize)
                    .mulScalar(cellSize)
                    .add(origin)
                    .subScalar(radius)
                    // .addScalar(cellSize * 0.5)
                    .subScalar(imageSize * 0.5);

                const { score, concept_body: { achievement } } = DATA[pointIndex++];
                const { difficulty_level, importance } = score;
                const { perfect_attempts: perfect } = achievement;

                // Content.promiseGlow
                //     .then(img => ctx2d.drawImage(img, position.x, position.y, imageSize, imageSize));

                if (importance > 5) {
                    Content.promiseGlow
                        .then(img => ctx2d.drawImage(img, position.x - imageSize / 2, position.y - imageSize / 2, imageSize * 2, imageSize * 2));
                }

                (difficulty_level > 5 ? Content.promiseHard : Content.promiseEasy)
                    .then(img => ctx2d.drawImage(img, position.x, position.y, imageSize, imageSize));

                if (perfect > 0) {
                    Content.promisePerfect
                        .then(img => ctx2d.drawImage(img, position.x, position.y - imageSize, imageSize, imageSize));
                }


                drawHexagon(position.x + imageSize / 2, position.y + imageSize / 2, imageSize / 4, "#0091ff");
            }
        });
}

redraw();

function onResize() { redraw(); }

window.addEventListener("resize", onResize, false);

//})