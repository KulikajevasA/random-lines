import Vector from "./vector";

class AStarSolver {
    /**
     * 
     * @param {*} graph 
     * @param {Vector} start 
     * @param {Vector} goal 
     */
    solve(untraversable, graph, vecStart, vecGoal) {
        const start = new NodeWrapper(vecStart);
        start.scoreF = this.getHeuristicScore(vecStart, vecGoal);
        start.scoreG = 0;

        const closed = [], open = [start];

        let current = null, exit = null;

        const eps = Math.pow(1e-5, 2);

        const maxIterations = 5000;
        let iterations = 0;

        while (open.length > 0) {
            current = open.sort((a, b) => a.scoreF - b.scoreF).shift();

            if (current.node.distanceToSqr(vecGoal) < eps || ++iterations > maxIterations) {
                exit = current;
                break;
            }

            closed.push(current);

            const { x, y } = current.node;

            let stepsInside = 0, prevNode = current;
            while (prevNode.entry) {
                const { x, y } = prevNode.node;
                const [currentValue, ,] = graph[x][y];

                if (currentValue < untraversable) stepsInside++;
                else break;

                prevNode = prevNode.entry;
            }

            for (let nx = Math.max(0, x - 1), gridWidth = graph.length; nx <= Math.min(gridWidth - 1, x + 1); nx++) {
                for (let ny = Math.max(0, y - 1), gridHeight = graph[nx].length; ny <= Math.min(gridHeight - 1, y + 1); ny++) {

                    if (nx === x && ny === y) continue;

                    const [value, , isValid] = graph[nx][ny];

                    if (!isValid) continue;

                    const other = new Vector(nx, ny);

                    if (closed.find(({ node }) => node.distanceToSqr(other) < eps))
                        continue;

                    const tentativeG = current.scoreG + other.distanceToSqr(current.node);
                    let neighbor = open.find(({ node }) => node.distanceToSqr(other) < eps);

                    if (!neighbor) {
                        neighbor = new NodeWrapper(other);
                        open.push(neighbor);
                    } else if (tentativeG > neighbor.scoreG)
                        continue;

                    const [currentValue, auraPenalty,] = graph[nx][ny];
                    const heuristicDistance = this.getHeuristicScore(neighbor.node, vecGoal);
                    const penalty = currentValue < untraversable
                        ? stepsInside + 2 //((untraversable - value) * heuristicDistance * stepsInside**2)
                        : 1;

                    neighbor.entry = current;
                    neighbor.scoreG = tentativeG;
                    neighbor.scoreF = neighbor.scoreG + Math.pow(heuristicDistance, penalty) * (1 + auraPenalty);
                }
            }
        }

        const path = this.reconstructPath(start, exit);

        return [!!exit, path];
    }

    reconstructPath(entry, exit) {
        const path = [];

        let prevNode = exit;
        while (prevNode.entry !== null) {
            path.push(prevNode.node);
            prevNode = prevNode.entry;
        }

        path.push(entry.node);

        return path;
    }

    /**
     * 
     * @param {Vector} v1 
     * @param {Vector} v2 
     */
    getHeuristicScore(v1, v2) { return v1.distanceToSqr(v2); }
}

export default AStarSolver;
export { AStarSolver };

class NodeWrapper {
    /**
     * 
     * @param {Vector} node 
     */
    constructor(node) {
        this.scoreG = Infinity;
        this.scoreF = 0;
        this.node = node;
        this.entry = null;
    }
}