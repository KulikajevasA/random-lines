class Vector {
    constructor(x = 0, y = 0) {
        this.x = this.w = x;
        this.y = this.h = y;

        Object.freeze(this);
    }

    toArray() { return [this.x, this.y]; }
    round() { return new Vector(Math.round(this.x), Math.round(this.y)); }

    add(vector) { return new Vector(this.x + vector.x, this.y + vector.y); }
    sub(vector) { return new Vector(this.x - vector.x, this.y - vector.y); }
    mul(vector) { return new Vector(this.x * vector.x, this.y * vector.y); }

    addScalar(scalar) { return new Vector(this.x + scalar, this.y + scalar); }
    subScalar(scalar) { return new Vector(this.x - scalar, this.y - scalar); }
    mulScalar(scalar) { return new Vector(this.x * scalar, this.y * scalar); }
    divScalar(scalar) { return new Vector(this.x / scalar, this.y / scalar); }

    length() { return Math.sqrt(this.lengthSqr()); }
    lengthSqr() { return this.x ** 2 + this.y ** 2; }
    distanceTo(vector) { return Math.sqrt(this.distanceToSqr(vector)); }
    distanceToSqr(vector) { return this.sub(vector).lengthSqr(); }

    lerp(vector, alpha) { return vector.sub(this).mulScalar(Math.max(Math.min(alpha, 1), 0)).add(this); }

    setLength(len) { return this.normalize().mulScalar(len); }
    normalize() { return this.divScalar(this.length()); }

    rotate(point, alpha) {
        const { x, y } = this.sub(point);
        const sinA = Math.sin(alpha), cosA = Math.cos(alpha);

        const x2 = cosA * x - sinA * y;
        const y2 = sinA * x + cosA * y;

        return new Vector(x2, y2).add(point);
    }
}

export default Vector;
export { Vector };