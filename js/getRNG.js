function getRNG(seed = null) {
    const random = seed === null ? Math.random : mulberry32(seed);

    return new class Random {
        next() { return random(); }
        get(min = 1, max) {
            if (!Number.isFinite(max)) {
                max = min;
                min = 0;
            }

            return random() * (max - min) + min;
        }
    }
}

export default getRNG;
export { getRNG };

function mulberry32(a = Date.now()) {
    return function () {
        var t = a += 0x6D2B79F5;
        t = Math.imul(t ^ t >>> 15, t | 1);
        t ^= t + Math.imul(t ^ t >>> 7, t | 61);
        return ((t ^ t >>> 14) >>> 0) / 4294967296;
    }
};