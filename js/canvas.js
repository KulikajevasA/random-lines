function findCanvas(canvasId) {
    /**
     * @type {HTMLCanvasElement}
     */
    const canvas = document.getElementById(canvasId);
    const ctx2d = canvas.getContext("2d");

    let canvasSize = 0;

    function onResize() {
        const actualSize = Math.min(window.innerWidth, window.innerHeight);

        canvasSize = actualSize * window.devicePixelRatio;

        canvas.setAttribute("width", canvasSize);
        canvas.setAttribute("height", canvasSize);

        canvas.setAttribute("style", `width: ${actualSize}px; height: ${actualSize}px`);
    }

    window.addEventListener("resize", onResize, false);
    onResize();

    return new class Canvas {
        get size() { return canvasSize; }
        get domElement() { return canvas; }
        get ctx2d() { return ctx2d; }
        erase() {
            ctx2d.clearRect(0, 0, canvasSize, canvasSize);

            return this;
        }
    };
}

export default findCanvas;
export { findCanvas };