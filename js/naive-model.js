import getRNG from "./getRNG";
import Vector from "./vector";

const TWO_PI = Math.PI * 2;

function drawModel({ canvas, seed, origin, radius }) {
    const { ctx2d } = canvas;
    const rng = getRNG(seed);
    const pointCount = 15, wanderRadius = radius * 0.4, steps = 1000, stepLen = wanderRadius / steps;

    function isPointInsideCircle(point, origin, radius) {
        const rSq = radius ** 2;
        const dSq = point.distanceToSqr(origin);

        return rSq > dSq;
    }

    function pickRandomPoint(origin, radius) {
        let newPoint, protection = 0;

        do {
            if (protection > 1000) throw new Error("Took too many iterations.");

            // newPoint = pickPoint(lastPoint, wanderRadius * 1.5);
            newPoint = new Vector(0, 1)
                .rotate(new Vector(), rng.get(TWO_PI))
                .mulScalar(rng.get(radius))
                .add(origin);
            protection++;
        } while (!isPointInsideCircle(newPoint, origin, radius));

        return newPoint;
    }

    function pickBondPoint({ x, y }, radius) { return new Vector(x + radius, y).rotate(new Vector(x, y), rng.next() * TWO_PI); }
    function drawCircle({ x, y }, radius, color) {
        ctx2d.beginPath();
        ctx2d.strokeStyle = color;
        ctx2d.arc(x, y, radius, 0, TWO_PI);
        ctx2d.stroke();
    }

    canvas.erase();

    for (let k = 0; k < 3; k++) {
        const color = `#${Math.floor(rng.get(0xffffff)).toString(16)}`;

        const firstPoint = pickBondPoint(origin, radius * 0.7);
        drawCircle(firstPoint, 10, "red");

        let lastDirection = firstPoint.sub(origin).normalize();
        let lastPoint = firstPoint;

        for (let i = 1; i < pointCount; i++) {
            let newPoint = null, protection = 0;
            do {
                if (protection > 1000) throw new Error("Took too many iterations.");

                newPoint = pickBondPoint(lastPoint, wanderRadius * 1.5);
                // newPoint = pickRandomPoint(origin, radius);
                protection++;
            } while (!isPointInsideCircle(newPoint, origin, radius - 10));

            let lastStep = lastPoint;

            for (let i = 1; i < steps; i++) {
                const dirDesired = newPoint.sub(lastStep).normalize();
                const direction = lastDirection.lerp(dirDesired, 0.7);

                lastStep = lastStep.add(direction.mulScalar(stepLen));

                drawCircle(lastStep, 3, color);
            }

            // drawCircle(newPoint, 3, "red");
            drawCircle(lastStep, 3, "red");
            lastPoint = lastStep;

            // break;
        }
    }

    drawCircle(origin, radius, "black");
    drawCircle(origin, 3, "magenta");
}

export default drawModel;
export { drawModel };