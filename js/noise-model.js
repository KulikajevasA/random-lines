import { Simplex2 as Noise } from "tumult";
import Box from "./box";
import AStarSolver from "./a-star";
import { CatmullRomCurve3 } from "./catmull-rom";
import Vector from "./vector";
import getRNG from "./getRNG";
import isPointInsideCircle from "./point-circle-intersection";

function createNoiseModel({ seed, gridDims, untraversable }) {
    const size = 100, center = size * 0.5;
    const origin = new Vector(center, center);
    const radius = center * 0.9;
    const cellSize = (radius * 2) / gridDims;

    const allCurves = [], allPoints = [];

    createFields().forEach((fieldValue, index) => {
        const rng = getRNG(seed);
        const minFieldValue = untraversable[index];
        const graph = createGraph(rng, fieldValue, minFieldValue);
        const linePoints = getLinePoints(fieldValue, graph, minFieldValue);
        const [curves, points] = getCurvesAndPoints(rng, linePoints);

        allCurves.push(...curves);
        allPoints.push(...points);
    });

    return new class NoiseModel {
        get curves() { return allCurves.slice() };
        get points() { return allPoints.slice(); }

        get origin() { return origin; }
        get radius() { return radius; }
    };

    function desnityFunc(value) { return Math.min(1, Math.pow(value + 0.2, 3)); }

    function createFields() {
        const perlin = new Noise(seed);

        const fieldValuesForward = new Array(gridDims);
        const fieldValuesBackward = new Array(gridDims);

        for (let i = 0; i < gridDims; i++) {
            fieldValuesForward[i] = new Array(gridDims);
            fieldValuesBackward[i] = new Array(gridDims);

            for (let j = 0; j < gridDims; j++) {
                const fieldValue = (perlin.gen(i / gridDims * 6 + 0.01, j / gridDims * 6 + 0.01) + 1) * 0.5;

                const position = new Vector(i, j)
                    .mulScalar(cellSize)
                    .add(origin)
                    .subScalar(radius);

                const isValid = isPointInsideCircle(
                    position.addScalar(cellSize * 0.5),
                    origin, radius - cellSize * 0.5
                );

                fieldValuesForward[i][j] = [fieldValue, false, isValid];
                fieldValuesBackward[i][j] = [1 - fieldValue, false, isValid];
            }
        }

        return [fieldValuesForward, fieldValuesBackward];
    }

    function findBlob(fieldValues, kx, ky) {
        let minX = Infinity, minY = Infinity;
        let maxX = -Infinity, maxY = -Infinity;

        const open = [[kx, ky]], nodes = [[kx, ky]];
        fieldValues[kx][ky][1] = true;

        while (open.length > 0) {
            const [x, y] = open.shift();

            minX = minX > x ? x : minX;
            minY = minY > y ? y : minY;
            maxX = maxX < x ? x : maxX;
            maxY = maxY < y ? y : maxY;

            for (let nx = Math.max(0, x - 1); nx <= Math.min(gridDims - 1, x + 1); nx++) {
                for (let ny = Math.max(0, y - 1); ny <= Math.min(gridDims - 1, y + 1); ny++) {
                    const [value, used,] = fieldValues[nx][ny];

                    if (used) continue;

                    const density = Math.round(desnityFunc(value) * 0xff);

                    if (density <= 250) continue;

                    fieldValues[nx][ny][1] = true;

                    open.push([nx, ny]);
                    nodes.push([nx, ny]);
                }
            }
        }

        const bbox = new Box(new Vector(minX, minY), new Vector(maxX, maxY));

        return [bbox, Object.freeze(nodes)];
    }

    function createGraph(rng, fieldValues, untraversable) {
        const graph = {};

        for (let i = 0; i < gridDims; i++) {
            for (let j = 0; j < gridDims; j++) {
                const [value, used, isValid] = fieldValues[i][j];

                if (!isValid || used) continue;

                if (value > untraversable) {

                    const density = Math.round(desnityFunc(value) * 0xff);

                    if (density <= 250) {
                        fieldValues[i][j][1] = true;
                    } else {
                        const [boundingBox,] = findBlob(fieldValues, i, j);
                        const bboxCenter = boundingBox.getCenter(), roundedBboxCenter = bboxCenter.round();

                        const center = bboxCenter
                            .mulScalar(cellSize)
                            .add(origin)
                            .subScalar(radius);

                        if (isPointInsideCircle(center, origin, radius - cellSize * 2) && fieldValues[roundedBboxCenter.x][roundedBboxCenter.y][0] >= 0.5) {

                            if (false || rng.next() > 0.3) {
                                graph[`${roundedBboxCenter.x}_${roundedBboxCenter.y}`] = {
                                    boundingBox,
                                    connections: {},
                                    maxConnections: rng.next() > 0.9 ? 1 : 2
                                };
                            }
                        }
                    }
                } else {
                    fieldValues[i][j][1] = true;
                }
            }
        }

        for (let i = 0; i < gridDims; i++)
            for (let j = 0; j < gridDims; j++)
                fieldValues[i][j][1] = 0;

        return Object.freeze(graph);
    }

    function getLinePoints(fieldValues, graph, untraversable) {
        const solver = new AStarSolver();
        const vertexList = Object.keys(graph);
        let changed = false, lastPoints = [];

        const allPoints = [lastPoints];

        do {
            changed = false;
            vertexList.forEach(vertexName => {
                const vertex = graph[vertexName];
                const connections = Object.keys(vertex.connections);
                const availableConnections = connections.length;

                if (availableConnections >= vertex.maxConnections) return;

                const center = vertex.boundingBox.getCenter();

                const loopConnections = { [vertexName]: true };

                function checkLoops(connections) {
                    connections.forEach(connectedVertex => {
                        if (connectedVertex in loopConnections) return;

                        loopConnections[connectedVertex] = true;

                        checkLoops(Object.keys(graph[connectedVertex].connections));
                    });
                }

                checkLoops(connections);

                const distances = vertexList
                    .filter(vertexName => {
                        const other = graph[vertexName];

                        if (other === vertex) return false;
                        if (vertexName in vertex.connections) return false;
                        if (vertexName in loopConnections) return false;

                        const availableConnections = Object.keys(other.connections).length;

                        if (availableConnections >= other.maxConnections) return false;

                        return true;
                    }).map(vertexName => {
                        const other = graph[vertexName];
                        const centerOther = other
                            .boundingBox
                            .getCenter();

                        const distance = centerOther.distanceToSqr(center);

                        return [vertexName, other, centerOther, distance];
                    }).sort(([, , , a], [, , , b]) => a - b);

                const other = distances.shift();

                if (!other) return;

                vertex.connections[other[0]] = true;
                other[1].connections[vertexName] = true;

                const start = center.round(), end = other[2].round();

                const [, path] = solver.solve(untraversable, fieldValues, start, end);

                const auraDistance = 0;
                path.forEach(vector => {
                    const { x, y } = vector;
                    const furthestDistance = new Vector(x + auraDistance * 2 + 1, y + auraDistance * 2 + 1).distanceTo(vector)

                    for (let nx = Math.max(0, x - auraDistance * 2); nx <= Math.min(gridDims - 1, x + auraDistance * 2); nx++) {
                        for (let ny = Math.max(0, y - auraDistance * 2); ny <= Math.min(gridDims - 1, y + auraDistance * 2); ny++) {
                            const penalty = Math.max(0, furthestDistance - new Vector(nx, ny).distanceTo(vector));

                            fieldValues[nx][ny][1] = penalty;
                        }
                    }
                });


                const lastDistance = lastPoints.length > 0 ? lastPoints.slice(-1)[0].distanceToSqr(path[0]) : 0;
                const firstDistance = lastPoints.length > 0 ? lastPoints.slice(0)[0].distanceToSqr(path.slice(-1)[0]) : 0;
                const maxDistance = 10, maxPoints = 100;
                const distanceBack = lastDistance < maxDistance * maxDistance;
                const distanceFront = firstDistance < maxDistance * maxDistance;

                if (lastPoints.length < maxPoints && (distanceBack || distanceFront)) {
                    if (distanceBack) lastPoints.push(...path);
                    else lastPoints.push(...path.reverse());
                }
                else {
                    lastPoints = path;
                    allPoints.push(path);
                }

                changed = true;
            });
        } while (changed);

        return Object.freeze(allPoints);
    }

    function getCurvesAndPoints(rng, linePoints) {
        const curves = [], points = [];

        linePoints.forEach(_points => {
            const sampledPoints = _points.filter(() => rng.next() > 0.5);

            const curve = new CatmullRomCurve3(sampledPoints, false, "chordal", 0.1);

            points.push(curve.getSpacedPoints(10000 - 1));

            curves.push([curve, curve.getLength()]);
        });


        return [Object.freeze(curves), Object.freeze(points)];
    }

}

export default createNoiseModel;
export { createNoiseModel };