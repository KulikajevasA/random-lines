const promisePerfect = new Promise(resolve => {
    const img = new Image();
    img.src = "content/perfect.svg"

    img.onload = () => resolve(img);
});

const promiseEasy = new Promise(resolve => {
    const img = new Image();
    img.src = "content/easy.svg"

    img.onload = () => resolve(img);
});


const promiseHard = new Promise(resolve => {
    const img = new Image();
    img.src = "content/difficult.svg"

    img.onload = () => resolve(img);
});

const promiseGlow = new Promise(resolve => {
    const img = new Image();
    img.src = "content/glow.png"

    img.onload = () => resolve(img);
});

export { promisePerfect, promiseEasy, promiseHard, promiseGlow };