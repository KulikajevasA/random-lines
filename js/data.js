const response = Object.freeze({
    "success": true,
    "data": [
        {
            "code": "new_KG2011",
            "sequence": 238,
            "display_name": "Covalent Bonding",
            "score": {
                "importance": 5.23,
                "difficulty_level": 6,
                "length": 3.79,
                "fundamental_score": 2.89,
                "complexity_score": 3.09,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 2,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2015",
            "sequence": 239,
            "display_name": "Covalent Bonding in Carbon",
            "score": {
                "importance": 3.79,
                "difficulty_level": 6,
                "length": 3.33,
                "fundamental_score": 1.95,
                "complexity_score": 2.87,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2016",
            "sequence": 240,
            "display_name": "Reason for Covalent Bonding in Carbon",
            "score": {
                "importance": 2.17,
                "difficulty_level": 2.8,
                "length": 2.87,
                "fundamental_score": 1.0,
                "complexity_score": 2.54,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2012",
            "sequence": 241,
            "display_name": "Single Covalent Bond",
            "score": {
                "importance": 6.04,
                "difficulty_level": 2.89,
                "length": 3.94,
                "fundamental_score": 1.24,
                "complexity_score": 2.89,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2020",
            "sequence": 242,
            "display_name": "Formation of Hydrogen Molecule",
            "score": {
                "importance": 1.27,
                "difficulty_level": 2.97,
                "length": 3.54,
                "fundamental_score": 1.0,
                "complexity_score": 3.55,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2013",
            "sequence": 243,
            "display_name": "Double Covalent Bond",
            "score": {
                "importance": 3.16,
                "difficulty_level": 2.71,
                "length": 4.68,
                "fundamental_score": 1.47,
                "complexity_score": 4.31,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2021",
            "sequence": 244,
            "display_name": "Formation of Oxygen Molecule",
            "score": {
                "importance": 1.36,
                "difficulty_level": 2.16,
                "length": 2.52,
                "fundamental_score": 1.24,
                "complexity_score": 2.42,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2014",
            "sequence": 245,
            "display_name": "Triple Covalent Bond",
            "score": {
                "importance": 2.89,
                "difficulty_level": 2.11,
                "length": 3.37,
                "fundamental_score": 1.47,
                "complexity_score": 3.01,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2022",
            "sequence": 246,
            "display_name": "Formation of Nitrogen Molecule",
            "score": {
                "importance": 1.54,
                "difficulty_level": 2.46,
                "length": 2.81,
                "fundamental_score": 1.47,
                "complexity_score": 2.6,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2019",
            "sequence": 247,
            "display_name": "Formation of Covalent Compounds",
            "score": {
                "importance": 4.6,
                "difficulty_level": 3.06,
                "length": 3.76,
                "fundamental_score": 1.95,
                "complexity_score": 2.92,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 2,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2017",
            "sequence": 248,
            "display_name": "Formation of Methane",
            "score": {
                "importance": 1.63,
                "difficulty_level": 1.94,
                "length": 2.95,
                "fundamental_score": 1.24,
                "complexity_score": 2.91,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2018",
            "sequence": 249,
            "display_name": "Electron Dot Structure of Methane",
            "score": {
                "importance": 1.81,
                "difficulty_level": 2.11,
                "length": 2.57,
                "fundamental_score": 1.0,
                "complexity_score": 2.46,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2023",
            "sequence": 250,
            "display_name": "Allotropes of Carbon",
            "score": {
                "importance": 4.96,
                "difficulty_level": 2.11,
                "length": 3.53,
                "fundamental_score": 1.95,
                "complexity_score": 2.76,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2024",
            "sequence": 251,
            "display_name": "Diamond",
            "score": {
                "importance": 5.23,
                "difficulty_level": 2.29,
                "length": 3.06,
                "fundamental_score": 2.42,
                "complexity_score": 2.12,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2026",
            "sequence": 252,
            "display_name": "Graphite",
            "score": {
                "importance": 4.69,
                "difficulty_level": 2.46,
                "length": 3.56,
                "fundamental_score": 1.95,
                "complexity_score": 2.75,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2028",
            "sequence": 253,
            "display_name": "Fullerenes",
            "score": {
                "importance": 2.8,
                "difficulty_level": 2.24,
                "length": 2.63,
                "fundamental_score": 1.95,
                "complexity_score": 2.24,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2030",
            "sequence": 254,
            "display_name": "Versatile Nature of Carbon",
            "score": {
                "importance": 2.17,
                "difficulty_level": 2.29,
                "length": 3.62,
                "fundamental_score": 3.37,
                "complexity_score": 3.45,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2031",
            "sequence": 255,
            "display_name": "Bonding of Carbon with Heteroatoms",
            "score": {
                "importance": 1.72,
                "difficulty_level": 1.94,
                "length": 2.8,
                "fundamental_score": 1.24,
                "complexity_score": 2.73,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36533",
            "sequence": 256,
            "display_name": "Halogens as Substituents",
            "score": {
                "importance": 1.0,
                "difficulty_level": 1.34,
                "length": 1.16,
                "fundamental_score": 1.47,
                "complexity_score": 1.01,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2032",
            "sequence": 257,
            "display_name": "Tetravalency of Carbon",
            "score": {
                "importance": 2.08,
                "difficulty_level": 2.07,
                "length": 2.18,
                "fundamental_score": 1.24,
                "complexity_score": 1.98,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 3,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2033",
            "sequence": 258,
            "display_name": "Multiple Bond Formation by Carbon",
            "score": {
                "importance": 1.36,
                "difficulty_level": 2.2,
                "length": 3.42,
                "fundamental_score": 1.47,
                "complexity_score": 3.31,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2034",
            "sequence": 259,
            "display_name": "Catenation Property of Carbon",
            "score": {
                "importance": 5.5,
                "difficulty_level": 2.29,
                "length": 3.98,
                "fundamental_score": 1.24,
                "complexity_score": 3.13,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2035",
            "sequence": 260,
            "display_name": "Compounds of Carbon",
            "score": {
                "importance": 2.44,
                "difficulty_level": 2.29,
                "length": 3.02,
                "fundamental_score": 6.21,
                "complexity_score": 2.65,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 2,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36801",
            "sequence": 261,
            "display_name": "Organic Compounds",
            "score": {
                "importance": 1.0,
                "difficulty_level": 1.34,
                "length": 1.16,
                "fundamental_score": 1.47,
                "complexity_score": 1.02,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2036",
            "sequence": 262,
            "display_name": "Saturated Hydrocarbons",
            "score": {
                "importance": 5.68,
                "difficulty_level": 2.71,
                "length": 5.07,
                "fundamental_score": 2.89,
                "complexity_score": 4.15,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2037",
            "sequence": 263,
            "display_name": "Electron Dot Structure of Ethane",
            "score": {
                "importance": 1.63,
                "difficulty_level": 2.54,
                "length": 1.9,
                "fundamental_score": 1.24,
                "complexity_score": 1.69,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2039",
            "sequence": 264,
            "display_name": "Unsaturated Carbon Compounds",
            "score": {
                "importance": 5.77,
                "difficulty_level": 3.06,
                "length": 3.22,
                "fundamental_score": 2.89,
                "complexity_score": 1.99,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2040",
            "sequence": 265,
            "display_name": "Electron Dot Structure of Ethene",
            "score": {
                "importance": 1.81,
                "difficulty_level": 2.54,
                "length": 2.19,
                "fundamental_score": 1.24,
                "complexity_score": 1.97,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2041",
            "sequence": 266,
            "display_name": "Electron Dot Structure of Ethyne",
            "score": {
                "importance": 1.9,
                "difficulty_level": 2.24,
                "length": 2.53,
                "fundamental_score": 1.47,
                "complexity_score": 2.32,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2043",
            "sequence": 267,
            "display_name": "Chain Structures of Hydrocarbons",
            "score": {
                "importance": 1.99,
                "difficulty_level": 2.29,
                "length": 3.34,
                "fundamental_score": 1.95,
                "complexity_score": 3.2,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2044",
            "sequence": 268,
            "display_name": "Structure of Methane",
            "score": {
                "importance": 2.08,
                "difficulty_level": 1.86,
                "length": 1.92,
                "fundamental_score": 1.0,
                "complexity_score": 1.7,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2045",
            "sequence": 269,
            "display_name": "Structure of Ethane",
            "score": {
                "importance": 2.08,
                "difficulty_level": 2.24,
                "length": 3.16,
                "fundamental_score": 1.0,
                "complexity_score": 3.06,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2046",
            "sequence": 270,
            "display_name": "Structure of Propane",
            "score": {
                "importance": 1.54,
                "difficulty_level": 1.99,
                "length": 1.71,
                "fundamental_score": 1.0,
                "complexity_score": 1.58,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2047",
            "sequence": 271,
            "display_name": "Chain Structure of Butane",
            "score": {
                "importance": 1.45,
                "difficulty_level": 2.54,
                "length": 2.91,
                "fundamental_score": 1.24,
                "complexity_score": 2.82,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2048",
            "sequence": 272,
            "display_name": "Chain Structure of Pentane",
            "score": {
                "importance": 1.99,
                "difficulty_level": 2.03,
                "length": 3.03,
                "fundamental_score": 1.24,
                "complexity_score": 2.84,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2049",
            "sequence": 273,
            "display_name": "Chain Structure of Hexane",
            "score": {
                "importance": 1.0,
                "difficulty_level": 1.51,
                "length": 2.45,
                "fundamental_score": 1.24,
                "complexity_score": 2.42,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2050",
            "sequence": 274,
            "display_name": "Branched Structure of C4H10",
            "score": {
                "importance": 1.45,
                "difficulty_level": 2.2,
                "length": 2.86,
                "fundamental_score": 1.47,
                "complexity_score": 2.7,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 2,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36539",
            "sequence": 275,
            "display_name": "Structural Isomerism",
            "score": {
                "importance": 4.15,
                "difficulty_level": 2.71,
                "length": 3.27,
                "fundamental_score": 1.95,
                "complexity_score": 2.49,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2051",
            "sequence": 276,
            "display_name": "Ring Structures of Hydrocarbons",
            "score": {
                "importance": 2.26,
                "difficulty_level": 2.41,
                "length": 2.1,
                "fundamental_score": 1.47,
                "complexity_score": 1.77,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2052",
            "sequence": 277,
            "display_name": "Ring Structure of Cyclohexane",
            "score": {
                "importance": 1.63,
                "difficulty_level": 2.63,
                "length": 3.42,
                "fundamental_score": 1.24,
                "complexity_score": 3.26,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2053",
            "sequence": 278,
            "display_name": "Ring Structure of Benzene",
            "score": {
                "importance": 2.35,
                "difficulty_level": 2.29,
                "length": 3.33,
                "fundamental_score": 1.24,
                "complexity_score": 3.1,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2054",
            "sequence": 279,
            "display_name": "Functional Group",
            "score": {
                "importance": 5.59,
                "difficulty_level": 2.89,
                "length": 3.72,
                "fundamental_score": 4.79,
                "complexity_score": 2.67,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2055",
            "sequence": 280,
            "display_name": "Functional Group in Alcohols",
            "score": {
                "importance": 3.16,
                "difficulty_level": 2.41,
                "length": 3.77,
                "fundamental_score": 1.71,
                "complexity_score": 3.32,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2056",
            "sequence": 281,
            "display_name": "Functional Group in Aldehydes",
            "score": {
                "importance": 3.25,
                "difficulty_level": 2.54,
                "length": 3.01,
                "fundamental_score": 1.71,
                "complexity_score": 2.47,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2057",
            "sequence": 282,
            "display_name": "Functional Group in Ketones",
            "score": {
                "importance": 3.61,
                "difficulty_level": 2.29,
                "length": 3.25,
                "fundamental_score": 1.71,
                "complexity_score": 2.65,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2058",
            "sequence": 283,
            "display_name": "Functional Group in Carboxylic Acids",
            "score": {
                "importance": 3.97,
                "difficulty_level": 2.71,
                "length": 2.62,
                "fundamental_score": 1.95,
                "complexity_score": 1.81,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2059",
            "sequence": 284,
            "display_name": "Homologous Series",
            "score": {
                "importance": 6.85,
                "difficulty_level": 2.37,
                "length": 3.76,
                "fundamental_score": 2.89,
                "complexity_score": 2.62,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2061",
            "sequence": 285,
            "display_name": "Properties of Homologous Series",
            "score": {
                "importance": 3.07,
                "difficulty_level": 3.4,
                "length": 3.79,
                "fundamental_score": 1.0,
                "complexity_score": 3.27,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2062",
            "sequence": 286,
            "display_name": "Homologous Series of Alkanes",
            "score": {
                "importance": 4.78,
                "difficulty_level": 2.46,
                "length": 3.51,
                "fundamental_score": 1.95,
                "complexity_score": 2.61,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36534",
            "sequence": 287,
            "display_name": "Homologous Series of Alkenes",
            "score": {
                "importance": 1.27,
                "difficulty_level": 2.63,
                "length": 1.29,
                "fundamental_score": 1.47,
                "complexity_score": 1.01,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36535",
            "sequence": 288,
            "display_name": "Homologous Series of Alkynes",
            "score": {
                "importance": 1.0,
                "difficulty_level": 1.86,
                "length": 1.24,
                "fundamental_score": 1.47,
                "complexity_score": 1.01,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36556",
            "sequence": 289,
            "display_name": "Homologous Series of Haloalkanes",
            "score": {
                "importance": 1.0,
                "difficulty_level": 1.34,
                "length": 1.16,
                "fundamental_score": 1.47,
                "complexity_score": 1.01,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36536",
            "sequence": 290,
            "display_name": "Homologous Series of Alcohols",
            "score": {
                "importance": 1.0,
                "difficulty_level": 1.34,
                "length": 1.08,
                "fundamental_score": 1.0,
                "complexity_score": 1.01,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36537",
            "sequence": 291,
            "display_name": "Homologous Series of Aldehydes and Ketones",
            "score": {
                "importance": 1.0,
                "difficulty_level": 1.34,
                "length": 3.27,
                "fundamental_score": 1.47,
                "complexity_score": 3.31,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36538",
            "sequence": 292,
            "display_name": "Homologous Series of Carboxylic Acids",
            "score": {
                "importance": 1.0,
                "difficulty_level": 1.34,
                "length": 1.16,
                "fundamental_score": 1.47,
                "complexity_score": 1.02,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2063",
            "sequence": 293,
            "display_name": "Nomenclature of Carbon Compounds Containing Functional Groups",
            "score": {
                "importance": 4.87,
                "difficulty_level": 1.94,
                "length": 3.01,
                "fundamental_score": 2.89,
                "complexity_score": 2.3,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2064",
            "sequence": 294,
            "display_name": "Rules for Naming of Carbon Compounds",
            "score": {
                "importance": 4.78,
                "difficulty_level": 2.71,
                "length": 4.22,
                "fundamental_score": 1.71,
                "complexity_score": 3.47,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36802",
            "sequence": 295,
            "display_name": "Identification of Parent Carbon Chain",
            "score": {
                "importance": 1.0,
                "difficulty_level": 1.34,
                "length": 3.05,
                "fundamental_score": 1.47,
                "complexity_score": 3.08,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2065",
            "sequence": 296,
            "display_name": "Prefix of Carbon Compound",
            "score": {
                "importance": 1.45,
                "difficulty_level": 1.69,
                "length": 3.06,
                "fundamental_score": 1.24,
                "complexity_score": 3.08,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2066",
            "sequence": 297,
            "display_name": "Suffix for a Functional Group",
            "score": {
                "importance": 1.09,
                "difficulty_level": 1.69,
                "length": 1.1,
                "fundamental_score": 1.24,
                "complexity_score": 1.01,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36526",
            "sequence": 298,
            "display_name": "Introduction to IUPAC Nomenclature",
            "score": {
                "importance": 1.09,
                "difficulty_level": 2.03,
                "length": 1.17,
                "fundamental_score": 1.24,
                "complexity_score": 1.01,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2067",
            "sequence": 299,
            "display_name": "IUPAC Nomenclature of Hydrocarbons",
            "score": {
                "importance": 6.76,
                "difficulty_level": 1.94,
                "length": 4.23,
                "fundamental_score": 1.24,
                "complexity_score": 3.22,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2068",
            "sequence": 300,
            "display_name": "Chemical Properties of Carbon Compounds",
            "score": {
                "importance": 2.17,
                "difficulty_level": 2.11,
                "length": 2.87,
                "fundamental_score": 3.84,
                "complexity_score": 2.64,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2069",
            "sequence": 301,
            "display_name": "Combustion of Carbon Compounds",
            "score": {
                "importance": 4.33,
                "difficulty_level": 2.54,
                "length": 2.79,
                "fundamental_score": 3.37,
                "complexity_score": 2.08,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 2,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2070",
            "sequence": 302,
            "display_name": "Combustion of Coal and Petroleum",
            "score": {
                "importance": 1.36,
                "difficulty_level": 1.94,
                "length": 3.22,
                "fundamental_score": 1.24,
                "complexity_score": 3.4,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2072",
            "sequence": 303,
            "display_name": "Burning of Carbon Compounds with a Flame",
            "score": {
                "importance": 4.06,
                "difficulty_level": 2.46,
                "length": 3.3,
                "fundamental_score": 1.47,
                "complexity_score": 2.65,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2073",
            "sequence": 304,
            "display_name": "Burning of Carbon Compounds without Flame",
            "score": {
                "importance": 1.36,
                "difficulty_level": 2.24,
                "length": 1.69,
                "fundamental_score": 1.0,
                "complexity_score": 1.61,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36530",
            "sequence": 305,
            "display_name": "Burning of Carbon Compounds with a Coloured Flame",
            "score": {
                "importance": 1.63,
                "difficulty_level": 2.71,
                "length": 2.63,
                "fundamental_score": 1.47,
                "complexity_score": 2.39,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2074",
            "sequence": 306,
            "display_name": "Formation of Coal and Petroleum",
            "score": {
                "importance": 2.89,
                "difficulty_level": 2.63,
                "length": 3.24,
                "fundamental_score": 1.47,
                "complexity_score": 2.81,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2076",
            "sequence": 307,
            "display_name": "Oxidation of Ethyl Alcohol by Strong Oxidising Agents",
            "score": {
                "importance": 4.15,
                "difficulty_level": 2.54,
                "length": 3.78,
                "fundamental_score": 1.24,
                "complexity_score": 3.2,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2078",
            "sequence": 308,
            "display_name": "Addition Reactions of Hydrocarbons",
            "score": {
                "importance": 7.12,
                "difficulty_level": 3.23,
                "length": 3.51,
                "fundamental_score": 1.71,
                "complexity_score": 2.02,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2079",
            "sequence": 309,
            "display_name": "Substitution Reactions of Hydrocarbons",
            "score": {
                "importance": 3.43,
                "difficulty_level": 2.71,
                "length": 3.06,
                "fundamental_score": 1.47,
                "complexity_score": 2.48,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2080",
            "sequence": 310,
            "display_name": "Ethanol",
            "score": {
                "importance": 4.24,
                "difficulty_level": 2.89,
                "length": 3.34,
                "fundamental_score": 5.74,
                "complexity_score": 2.55,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36531",
            "sequence": 311,
            "display_name": "Sugarcane as a Source of Ethanol",
            "score": {
                "importance": 1.27,
                "difficulty_level": 1.69,
                "length": 2.17,
                "fundamental_score": 1.24,
                "complexity_score": 2.16,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36803",
            "sequence": 312,
            "display_name": "Physical Properties of Ethanol",
            "score": {
                "importance": 1.09,
                "difficulty_level": 2.29,
                "length": 2.87,
                "fundamental_score": 1.24,
                "complexity_score": 2.87,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2081",
            "sequence": 313,
            "display_name": "Chemical Properties of Ethanol",
            "score": {
                "importance": 4.6,
                "difficulty_level": 3.06,
                "length": 4.38,
                "fundamental_score": 2.42,
                "complexity_score": 3.42,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2082",
            "sequence": 314,
            "display_name": "Reaction of Ethanol with Sodium",
            "score": {
                "importance": 3.88,
                "difficulty_level": 2.71,
                "length": 2.91,
                "fundamental_score": 1.71,
                "complexity_score": 2.24,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 2,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2083",
            "sequence": 315,
            "display_name": "Dehydration of Ethanol",
            "score": {
                "importance": 3.25,
                "difficulty_level": 2.54,
                "length": 3.36,
                "fundamental_score": 1.24,
                "complexity_score": 2.93,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2084",
            "sequence": 316,
            "display_name": "Effects of Alcohols on Living Beings",
            "score": {
                "importance": 2.26,
                "difficulty_level": 2.37,
                "length": 4.36,
                "fundamental_score": 1.47,
                "complexity_score": 4.24,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36804",
            "sequence": 317,
            "display_name": "Absolute Alcohol",
            "score": {
                "importance": 1.0,
                "difficulty_level": 1.86,
                "length": 1.31,
                "fundamental_score": 1.71,
                "complexity_score": 1.01,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36532",
            "sequence": 318,
            "display_name": "Denaturation of Ethanol",
            "score": {
                "importance": 1.36,
                "difficulty_level": 2.2,
                "length": 2.78,
                "fundamental_score": 1.24,
                "complexity_score": 2.79,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2085",
            "sequence": 319,
            "display_name": "Use of Alcohol as a Fuel",
            "score": {
                "importance": 1.36,
                "difficulty_level": 2.71,
                "length": 2.86,
                "fundamental_score": 1.47,
                "complexity_score": 2.74,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2086",
            "sequence": 320,
            "display_name": "Ethanoic Acid",
            "score": {
                "importance": 5.59,
                "difficulty_level": 3.14,
                "length": 3.85,
                "fundamental_score": 6.21,
                "complexity_score": 2.55,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2087",
            "sequence": 321,
            "display_name": "Properties of Ethanoic Acid",
            "score": {
                "importance": 2.71,
                "difficulty_level": 3.06,
                "length": 3.13,
                "fundamental_score": 1.95,
                "complexity_score": 2.54,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 3,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2089",
            "sequence": 322,
            "display_name": "Esterification Reaction of Ethanoic Acid",
            "score": {
                "importance": 6.76,
                "difficulty_level": 3.06,
                "length": 4.54,
                "fundamental_score": 1.71,
                "complexity_score": 3.31,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2090",
            "sequence": 323,
            "display_name": "Reaction of Ethanoic Acid with a Base",
            "score": {
                "importance": 1.9,
                "difficulty_level": 2.37,
                "length": 2.33,
                "fundamental_score": 1.47,
                "complexity_score": 2.1,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2091",
            "sequence": 324,
            "display_name": "Reaction of Ethanoic Acid with Carbonates",
            "score": {
                "importance": 2.62,
                "difficulty_level": 2.37,
                "length": 3.03,
                "fundamental_score": 1.47,
                "complexity_score": 2.72,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2092",
            "sequence": 325,
            "display_name": "Reaction of Ethanoic Acid with Hydrogencarbonates",
            "score": {
                "importance": 4.06,
                "difficulty_level": 2.37,
                "length": 2.55,
                "fundamental_score": 1.71,
                "complexity_score": 1.88,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2094",
            "sequence": 326,
            "display_name": "Soaps and Detergents",
            "score": {
                "importance": 10.0,
                "difficulty_level": 5.29,
                "length": 4.9,
                "fundamental_score": 1.95,
                "complexity_score": 2.43,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2095",
            "sequence": 327,
            "display_name": "Micelles",
            "score": {
                "importance": 1.18,
                "difficulty_level": 2.24,
                "length": 1.9,
                "fundamental_score": 1.95,
                "complexity_score": 1.87,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2096",
            "sequence": 328,
            "display_name": "Structure of Micelles",
            "score": {
                "importance": 1.99,
                "difficulty_level": 2.37,
                "length": 1.79,
                "fundamental_score": 1.0,
                "complexity_score": 1.58,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2097",
            "sequence": 329,
            "display_name": "Formation of Micelles",
            "score": {
                "importance": 1.81,
                "difficulty_level": 2.11,
                "length": 2.32,
                "fundamental_score": 1.0,
                "complexity_score": 2.19,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2098",
            "sequence": 330,
            "display_name": "Mechanism of Cleansing Action of Soaps and Detergents",
            "score": {
                "importance": 1.99,
                "difficulty_level": 2.63,
                "length": 2.67,
                "fundamental_score": 1.0,
                "complexity_score": 2.53,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 2,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2093",
            "sequence": 331,
            "display_name": "Cleansing Agents",
            "score": {
                "importance": 1.63,
                "difficulty_level": 1.94,
                "length": 2.25,
                "fundamental_score": 2.42,
                "complexity_score": 2.16,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG2099",
            "sequence": 332,
            "display_name": "Applications of Cleansing Agents",
            "score": {
                "importance": 1.63,
                "difficulty_level": 1.86,
                "length": 3.06,
                "fundamental_score": 1.47,
                "complexity_score": 2.95,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": 1,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36805",
            "sequence": 333,
            "display_name": "Cleansing Mechanism of Soap with Hard Water",
            "score": {
                "importance": 1.27,
                "difficulty_level": 2.11,
                "length": 1.21,
                "fundamental_score": 1.47,
                "complexity_score": 1.01,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36806",
            "sequence": 334,
            "display_name": "Cleansing Mechanism of Detergents with Hard Water",
            "score": {
                "importance": 1.09,
                "difficulty_level": 2.63,
                "length": 1.17,
                "fundamental_score": 1.47,
                "complexity_score": 1.01,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36807",
            "sequence": 335,
            "display_name": "Advantages and Disadvantages of Soaps",
            "score": {
                "importance": 1.0,
                "difficulty_level": 1.34,
                "length": 1.16,
                "fundamental_score": 1.47,
                "complexity_score": 1.01,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        },
        {
            "code": "new_KG36808",
            "sequence": 336,
            "display_name": "Advantages and Disadvantages of Detergents",
            "score": {
                "importance": 1.0,
                "difficulty_level": 1.34,
                "length": 1.16,
                "fundamental_score": 1.47,
                "complexity_score": 1.01,
                "oswaal_importance": 1
            },
            "concept_body": {
                "achievement": {
                    "concept_mastery": null,
                    "perfect_attempts": 0
                },
                "progress": {
                    "question_coverage": {
                        "attempted_questions": 0,
                        "total_questions": 0
                    },
                    "video_coverage": {
                        "total_videos": null,
                        "watched_videos": 0
                    }
                }
            }
        }
    ]
});

const data = response.data;

export default data;
export { data };