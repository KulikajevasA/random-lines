function isPointInsideCircle(point, origin, radius) {
    const rSq = radius ** 2;
    const dSq = point.distanceToSqr(origin);

    return rSq > dSq;
}

export default isPointInsideCircle;
export { isPointInsideCircle };