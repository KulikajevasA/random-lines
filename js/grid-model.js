import getRNG from "./getRNG";
import Vector from "./vector";

const TWO_PI = Math.PI * 2;

function drawModel({ canvas, seed, origin, radius }) {
    const { ctx2d } = canvas;
    const rng = getRNG(seed);

    canvas.erase();

    function drawCircle({ x, y }, radius, color) {
        ctx2d.beginPath();
        ctx2d.strokeStyle = color;
        ctx2d.arc(x, y, radius, 0, TWO_PI);
        ctx2d.stroke();
    }

    drawCircle(origin, radius, "black");
}

export default drawModel;
export { drawModel };