const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
    entry: path.resolve(__dirname, "./js/main.js"),
    mode: "development",
    optimization: { minimize: false, usedExports: true },
    module: {
        rules: []
    },
    plugins: [
        new CopyWebpackPlugin({
            patterns: [
                { from: "./html", to: "" },
                { from: "./content", to: "content" }
            ]
        })
    ],
    output: {
        filename: "app.bundle.js",
        path: path.resolve(__dirname, "./bin")
    },
    devServer: {
        port: 8080
    },
    devtool: "source-map"
}